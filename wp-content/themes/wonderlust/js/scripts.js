var $ = jQuery.noConflict();

$(document).ready(function(){
	$('#nav_toggle').on('click', function(){
		$('.menu_overlay').slideToggle().css('z-index', 3);
	});

	$('#search_toggle').on('click', function(){
		$('#nav_search').toggleClass('active');
		$('#s').focus();
		if($('#nav_search').hasClass('active') && $('.menu_overlay').is(':visible')) {
			$('.menu_overlay').slideToggle();
		}
	});

	// if (getCookie('newsletter')) {
	//  // have cookie
	// } else {
	// 	setTimeout(function() {
	// 		$('body').addClass('display_newsletter');
	//  	 	setCookie('newsletter', 1);
	// 	}, 10 * 1000)
	// }
	

	$('.close_popup_newsletter').on('click', function(e) {
		e.preventDefault();
		$('body').removeClass('display_newsletter');
		$('.newsletter_email').val('');
	});

	if($('.newsletter_submit').length){
    $('.newsletter_submit').on('click', function(e) {
      e.preventDefault();
      $('.newsletter_email').removeClass('error');
      var submitParent = $(this).closest('.newsletter_content');
			var email = $('.newsletter_email', submitParent).val();
      if(email.length && isEmail(email)){
				$(this).val('Submitting');
        $('.signup_container').addClass('loading');
        signup_user(email, submitParent);
      } else {
        $('.newsletter_email').addClass('error');
      }
    });
  }

});

function signup_user(email, parent) {
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn' : 'sign_up_user',
      'email' : email
    }
  }).done( function (response) {
    $('.signup_container', parent).removeClass('loading');
    response = $.parseJSON(response);
    $(parent).html('<div class="response-text">'+response.response+'</div>');
    //$('.newsletter_email', parent).val('');
  });
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + 1268000000); //1 month
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}
