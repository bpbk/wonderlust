<div class="clear"></div>
</div>
<div id="footer_newsletter">
	<div class="footer_newsletter_container">
		<?php if(get_field('newsletter_image', 'option')){
			$image = get_field('newsletter_image', 'option');
		?>
			<div class="newsletter_image">
				<img src="<?php echo $image['sizes']['small-medium']; ?>"/>
			</div>
		<?php } ?>
		<div class="newsletter_content">
			<div class="newsletter_intro">
				<h2><?php the_field('newsletter_hed', 'option'); ?></h2>
				<div class="newsletter_dek">
					<?php the_field('newsletter_dek', 'option'); ?>
				</div>
			</div>
			<div class="signup_container">
				<?php if(get_field('newsletter_cta', 'option')){ ?>
					<div class="newsletter_cta">
						<?php the_field('newsletter_cta', 'option'); ?>
					</div>
				<?php } ?>
				<input class="newsletter_email" type="email" placeholder="EMAIL ADDRESS" />
				<input class="newsletter_submit" type="submit" value="submit" />
			</div>
		</div>
	</div>
</div>

<div id="popup_newsletter">
	<div id="popup_newsletter_wrapper">
		<div id="popup_newsletter_container">
			<?php if(get_field('popup_newsletter_image', 'option')){
				$image = get_field('popup_newsletter_image', 'option');
			?>
				<div class="newsletter_image bg_centered" style="background-image:url(<?php echo $image['sizes']['small-medium']; ?>);">
				</div>
			<?php } ?>
			<div class="newsletter_content">
				<div class="newsletter_intro">
					<h2><?php the_field('popup_newsletter_hed', 'option'); ?></h2>
					<div class="newsletter_dek">
						<?php the_field('popup_newsletter_dek', 'option'); ?>
					</div>
				</div>
				<div class="signup_container">
					<input class="newsletter_email" type="email" placeholder="Email" />
					<input class="newsletter_submit" type="submit" value="Subscribe" />
				</div>
				<div class="newsletter_signup_text">
				</div>
			</div>
			<a href="#" class="close_popup_newsletter">
			</a>
		</div>
	</div>
</div>
<!--
<?php if(!is_front_page()){ ?>
	<div id="zergnet-widget-56786"></div>

	<script language="javascript" type="text/javascript">
	    (function() {
	        var zergnet = document.createElement('script');
	        zergnet.type = 'text/javascript'; zergnet.async = true;
	        zergnet.src = (document.location.protocol == "https:" ? "https:" : "http:") + '//www.zergnet.com/zerg.js?id=56786';
	        var znscr = document.getElementsByTagName('script')[0];
	        znscr.parentNode.insertBefore(zergnet, znscr);
	    })();
	</script>
<?php } ?>
-->

<footer id="footer" role="contentinfo">
	<div class="inner_content">
		<div id="footer_content">
			<div class="footer_section about_us">
				<h6>ABOUT US</h6>
				<div class="footer_section_content">
					<img class="white_logo" src="<?php echo get_template_directory_uri(); ?>/images/white_logo.jpg"/>
					<?php wp_nav_menu( array('menu' => 'footer_menu') ); ?>
				</div>
			</div>
			<div class="footer_section">
				<h6>PRESS PLAY</h6>
				<div class="footer_section_content">
					<div class="press_play_container">
						<?php
						$pressPlay = get_field('press_play_posts', 'option');
						foreach($pressPlay as $pp){
							$image = wp_get_attachment_image_url( get_post_thumbnail_id( $pp ) );
						?>
							<div class="press_play">
								<div class="press_play_image_container">
									<a href="<?php echo get_the_permalink($pp); ?>">
										<div class="image_sizer">
										</div>
										<div class="press_play_image bg_centered" style="background-image:url(<?php echo $image; ?>);">
										</div>
									</a>
								</div>
								<div class="press_play_hed">
									<p><a href="<?php echo get_the_permalink($pp); ?>"><?php echo get_the_title( $pp ); ?></a></p>
								</div>
							</div>
						<?php
						}
						?>
					</div>
				</div>
			</div>
			<div class="footer_section">
				<h6>INSTAGRAM TODAY</h6>
				<div class="footer_section_content">
					<?php echo do_shortcode( '[instagram-feed]' ); ?>
				</div>
			</div>
			<div class="mobile footer_section about_us">
				<div class="footer_section_content">
					<img class="white_logo" src="<?php echo get_template_directory_uri(); ?>/images/white_logo.jpg"/>
					<?php wp_nav_menu( array('menu' => 'footer_menu') ); ?>
				</div>
			</div>
		</div>
	</div>
	<div id="copyright">
		Users of this site agree to its <a href="http://wonderlusttravel.com/terms-and-conditions/">Terms and Conditions</a> and <a href="http://wonderlusttravel.com/privacy-policy/">Privacy Policy</a>. ©2017 WONDERLUST. All Rights Reserved
	</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
