<?php get_header(); ?>
<?php
	$terms = get_the_terms( get_the_id(), 'post-type' );
	$cats = get_the_terms( get_the_id(), 'category' );
	$postID = get_the_id();
	if($terms[0]->slug != 'photo-essay' && !wp_is_mobile()){ ?>
<!--
	<div id="trending_bar">
		<div id="trending_bar_content">
			<strong>TRENDING</strong>  Rome, Cuba, Paris, Mexico City, Quito, Copenhagen, Lapland
		</div>
	</div>
-->
<?php }
	if(get_post_type() === 'homepage-section' && is_preview()) {
		show_homepage_sections(get_the_id());
	}
?>
<div class="inner_content">
	<section id="content" role="main">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			$thumbID = get_post_thumbnail_id();
			$image = wp_get_attachment_image_src( $thumbID, 'large' );
			$link = get_the_permalink();
			?>
			<header id="single_header" class="<?php echo $image[1] < $image[2] ? 'port_img' : 'horz_img'; ?>">
				<div id="single_header_content">
					<?php
					if($terms[0]->slug != 'photo-essay' && (!get_field('video_embed') && $terms[0]->slug != 'video')){
						$featID = get_post_thumbnail_id();
						$featImage = wp_get_attachment_image_url( get_post_thumbnail_id(), 'large' );
						$imageMeta = get_post($featID);
						$imageCredit = $imageMeta->post_content;
						$imageCaption = wp_get_attachment_caption( $featID );
						?>
						<div class="single_header_title_container">
							<div class="single_header_title">
								<?php
								if($terms[0]->slug == 'photo-essay'){ ?>
									<p class="cat_label">Portfolio</p>
								<?php
								}
								$catSlugs = array();
								$parentCatSlugs = array();
								if(isset($cats)){
									foreach($cats as $cat){
										array_push($catSlugs, $cat->slug);
									}
								}
								$parentID = wp_get_post_parent_id($postID);
								if($parentID != 0){
									$parentTerms = get_the_terms($parentID, 'category');
									if(isset($parentTerms)){
										foreach($parentTerms as $pTerms) {
											array_push($parentCatSlugs, $pTerms->slug);
										}
									}
								}else{
									$parentID = $postID;
								}
								if((isset($cats) && in_array('city', $catSlugs)) || (isset($parentCatSlugs) && in_array('city', $parentCatSlugs) )) {
									if(get_field('associated_city', $parentID)){ ?>
										<!--<p class="cat_label"><?php echo get_field('associated_city', $parentID); ?></p>-->
									<?php
									}
								} ?>
								<h1>
									<?php echo wl_title(); ?>
								</h1>
								<?php if(has_excerpt()){ ?>
									<div class="content_dek<?php echo get_field('all_caps_dek') ? ' all-caps-dek' : ''; ?>">
										<?php
										the_excerpt(); ?>
									</div>
								<?php
								}
							 ?>
							</div>
						</div>
						<?php if(has_post_thumbnail()) { ?>
							<div class="featured_thumbnail_container">
								<div class="featured_thumbnail_content">
									<div class="image_sizer"></div>
									<div class="featured_thumbnail bg_centered" style="background-image:url(<?php echo $featImage; ?>);">
									</div>
								</div>
								<div class="featured_thumbnail_info">
									<?php
									if(isset($imageCaption) && has_post_thumbnail()){ ?>
										<div class="featured_image_caption">
											<?php echo $imageCaption; ?>
										</div>
									<?php
									}
									if(isset($imageCredit) && has_post_thumbnail()){ ?>
										<div class="featured_image_credit">
											<?php echo $imageCredit; ?>
										</div>
									<?php
									} ?>
								</div>
							</div>
						<?php
						}
					}
					if(get_field('video_embed') && $terms[0]->slug == 'video'){ ?>
						<div class="single_header_title_container">
							<div class="single_header_title">
								<h1>
									<?php echo wl_title(); ?>
								</h1>
								<?php if(has_excerpt()){ ?>
									<div class="content_dek<?php echo get_field('all_caps_dek') ? ' all-caps-dek' : ''; ?>">
										<?php
										the_excerpt(); ?>
									</div>
								<?php
								} ?>
							</div>
						</div>
						<div class="post_video">
							<?php echo apply_filters('the_content', get_field('video_embed')); ?>
						</div>
					<?php
					} ?>


				</div>

			</header>
			<div id="single_content_container">
				<div id="single_content">
					<div class="single_content_text">
					<?php // ECHO THE CONTENT FOR ALL POSTS
					if(!get_field('hide_author')){
						if ( function_exists( 'coauthors_posts_links' ) ) {
							$author = coauthors_posts_links(', ', ' and ', '<div class="post_author single_post_author">By ', '</div>');
						}else{
							echo '<p class="single_post_author">By '.get_the_author_posts_link().'</p>';
						}
					}
					the_content(); ?>
					</div>
					<?php
					if($terms[0]->slug != 'photo-essay'){

						get_social_share();
					}
					// TOP LIST
/*
						if( have_rows('top_list') ): ?>
							<div class="top_list_container">
								<ol class="top_list">
									<?php
									while ( have_rows('top_list') ) : the_row(); ?>
										<li>
											<h3 class="top_list_title"><?php the_sub_field('list_item_title'); ?></h3>
											<?php the_sub_field('list_item_content'); ?>
										</li>
									<?php
									endwhile; ?>
								</ol>
							</div>
						<?php
						endif;
*/
					// END TOP LIST
					// PHOTO ESSAY
						if( have_rows('pe_images') ): ?>
							<div class="photo_essay_container">
								<?php
								while ( have_rows('pe_images') ) : the_row();
									$postTitle = get_sub_field('pe_title');
									$postSlug = implode('-', explode(' ', $postTitle)); ?>
									<a name="<?php echo $postSlug; ?>"></a>
									<div class="photo_essay">
										<div class="photo_essay_image">
											<?php
											if(get_sub_field('pe_image')){
												$image = get_sub_field('pe_image'); ?>
												<img src="<?php echo $image['sizes']['large']; ?>"/>
											<?php }
											if(get_sub_field('use_embed_instead') && get_sub_field('embed_image')){
												the_sub_field('embed_image');
											} ?>
										</div>
										<div class="photo_essay_content">

											<?php if(get_sub_field('pe_title')){
												$postTitle = get_sub_field('pe_title');
											?>
												<h3><?php the_sub_field('pe_title'); ?></h3>
											<?php } ?>
											<?php the_sub_field('pe_caption'); ?>
											<div class="photo_essay_social">
												<?php get_social_share(false, $postTitle, $link.'#'.$postSlug); ?>
											</div>
										</div>
									</div>
								<?php
								endwhile; ?>
							</div>
						<?php
						endif;
					// END PHOTO ESSAY
					?>
					<?php if($terms[0]->slug == 'photo-essay'){ ?>
						<div id="trending_bar">
							<div id="trending_bar_content">
								<strong>TRENDING</strong>  Rome, Cuba, Paris, Mexico City, Quito, Copenhagen, Lapland
							</div>
						</div>
					<?php } ?>
					<footer class="footer">
						<?php get_related_posts(get_queried_object_id(), false, true, 3); ?>
					</footer>
				</div>
				<?php
				if( $terms[0]->slug != 'photo-essay'){
					get_sidebar();
				} ?>
			</div>
		<?php
		endwhile; endif; ?>
		<?php if(wp_is_mobile()){ ?>
			<div id="trending_bar">
				<div id="trending_bar_content">
					<strong>TRENDING</strong>  Rome, Cuba, Paris, Mexico City, Quito, Copenhagen, Lapland
				</div>
			</div>
		<?php } ?>
	</section>
</div>
<script>
	jQuery('.wp-caption-text, .featured_image_caption').each(function(){
		var credit = jQuery(this).text();
		if (credit.indexOf('***') >= 0){
			credit = credit.split('***');
			console.log(credit);
			jQuery('<p class="photo_credit">'+credit[1]+'</p>').insertBefore(jQuery(this));
			jQuery(this).text(credit[0]);
			jQuery(this).parent().addClass('has_photo_credit');
		}
	});
</script>
<?php get_footer(); ?>
