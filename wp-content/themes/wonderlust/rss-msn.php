<?php
/**
 * Template Name: Custom RSS Template - msn
 */
header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        xmlns:media="http://search.yahoo.com/mrss/" 
        xmlns:mi="http://schemas.ingestion.microsoft.com/common/"
        <?php do_action('rss2_ns'); ?>>
  <channel>
    <title><?php bloginfo_rss('name'); ?> Feed</title>
    <language>en-us</language>
    <!-- ADDED THIS LINE -->
    	<atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
    <!-- (END) ADDED THIS LINE -->
    <link><?php bloginfo_rss('url') ?></link>
	<?php 
	do_action('rss2_head');
	$rssArgs = array('post_type' => 'post', 'posts_per_page' => -1);
	$rssFeed = new wp_query( $rssArgs );
	if( $rssFeed->have_posts() ):
		while( $rssFeed->have_posts() ): $rssFeed->the_post();
			$id = get_the_id();
			$content = get_the_content();
			$content = preg_replace("/<embed[^>]+\>/i", "(embed) ", $content); 		  
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]>', $content);
			$char_count = strlen( strip_tags( get_the_content() ) );
			$tags = get_the_tags();
			$images = get_attached_media( 'image' );
			$tagIds = wp_get_post_terms( $id, 'post_tag', array('fields' => 'ids') );
			$tagArgs = array(
			    'post__not_in'        => array( get_the_id() ),
			    'posts_per_page'      => 4,
			    'ignore_sticky_posts' => 1,
			    'orderby'             => 'rand',
			    'tax_query' => array(
			        array(
			            'taxonomy' => 'post_tag',
			            'terms'    => $tagIds
			        )
			    )
			);
			if ($char_count >= 450 || have_rows('pe_images')){ ?>
			    <item>
					<title><?php the_title_rss(); ?></title>
					<link><?php the_permalink_rss(); ?></link>
					<pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
					<guid isPermaLink="false"><?php the_guid(); ?></guid>
					<description><![CDATA[<?php echo $content; ?>]]></description>
					<?php if(has_excerpt()){ ?> 
						<dc:alternative>
						    <?php the_excerpt(); ?>
						</dc:alternative>
					<?php } ?>
					<dc:creator><?php the_author(); ?></dc:creator>
					<dc:modified><?php the_modified_date(); ?></dc:modified>
					<?php
					if($tags){
						$tagArray = array();
						foreach($tags as $tag){
							array_push($tagArray, $tag->name);
						}
						$tagString = implode(', ', $tagArray); ?>
						<media:keywords><?php echo $tagString; ?></media:keywords>
					<?php
					}
					if(get_the_post_thumbnail()){
						$thumb = wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); 
						?>
						<media:content url="<?php echo $thumb; ?>">
					        <media:thumbnail url="<?php echo $thumb; ?>" type="image/jpeg" />
					        <?php if(get_the_post_thumbnail_caption()){ 
						        $caption = get_the_post_thumbnail_caption();
						        if($credit = get_string_between($caption, '***', '***')){ ?>
							        <media:credit><?php echo $credit; ?></media:credit>
						        <?php
							    } 
							    if($captionOnly = preg_replace("/\*(.*?)\*/","",$caption)){ ?>
					        		<media:title><?php echo $captionOnly; ?></media:title>
								<?php 
								}
							} ?>
						</media:content>
					<?php
			        }
					if( have_rows('pe_images') ): ?>
						<media:group1>
						<?php
						while ( have_rows('pe_images') ) : the_row();
							$postTitle = get_sub_field('pe_title');
							$postSlug = implode('-', explode(' ', $postTitle));
							if(get_sub_field('pe_image')){
								$image = get_sub_field('pe_image'); ?>
								<media:content url="<?php echo $image['sizes']['large']; ?>" type="image/jpeg">
									<media:thumbnail url="<?php echo $image['sizes']['large']; ?>" type="image/jpeg" />
									<?php if(get_sub_field('pe_title')){ ?>
										<media:title><?php echo get_sub_field('pe_title'); ?></media:title>
									<?php } ?>
									<?php if(get_sub_field('pe_caption')){ ?>
										<media:text><?php echo get_sub_field('pe_caption'); ?></media:text>
									<?php } ?>
								</media:content>
							<?php }
						endwhile; ?>
						</media:group1>
					<?php
					endif;
					$related_query = new wp_query( $tagArgs );
					if( $related_query->have_posts() ):
						while( $related_query->have_posts() ): $related_query->the_post(); ?>
							<link rel="related" type="text/html" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php 
							if(get_the_post_thumbnail()){
								$thumb = wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); 
								?>
						        <media:thumbnail url="<?php echo $thumb; ?>" type="image/jpeg" />
						        <?php if(get_the_post_thumbnail_caption()){ 
							        $caption = get_the_post_thumbnail_caption();
							        if($credit = get_string_between($caption, '***', '***')){ ?>
								        <media:credit><?php echo $credit; ?></media:credit>
							        <?php
								    } 
								    if($captionOnly = preg_replace("/\*(.*?)\*/","",$caption)){ ?>
						        		<media:title><?php echo $captionOnly; ?></media:title>
									<?php 
									}
								}
					        } ?>
					      </link>
						<?php
						endwhile;
						wp_reset_postdata();
					endif;
					rss_enclosure(); ?>
	                <?php do_action('rss2_item'); ?>
				</item>    
			<?php 
			}
		endwhile;
		wp_reset_postdata();
	endif; ?> 
  </channel>
</rss>