/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(2);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

var $ = jQuery.noConflict();

$(document).ready(function () {
  $('#nav_toggle').on('click', function () {
    $('.menu_overlay').slideToggle().css('z-index', 3);
  });

  $('#search_toggle').on('click', function () {
    $('#nav_search').toggleClass('active');
    $('#s').focus();
    if ($('#nav_search').hasClass('active') && $('.menu_overlay').is(':visible')) {
      $('.menu_overlay').slideToggle();
    }
  });

  if (getCookie('newsletter')) {
    // have cookie
  } else {
    setTimeout(function () {
      $('body').addClass('display_newsletter');
      setCookie('newsletter', 1);
    }, 10 * 1000);
  }

  $('.close_popup_newsletter').on('click', function (e) {
    e.preventDefault();
    $('body').removeClass('display_newsletter');
    $('.newsletter_email').val('');
  });

  if ($('.newsletter_submit').length) {
    $('.newsletter_submit').on('click', function (e) {
      e.preventDefault();
      $('.newsletter_email').removeClass('error');
      var submitParent = $(this).closest('.newsletter_content');
      var email = $('.newsletter_email', submitParent).val();
      if (email.length && isEmail(email)) {
        $(this).val('Submitting');
        $('.signup_container').addClass('loading');
        signup_user(email, submitParent);
      } else {
        $('.newsletter_email').addClass('error');
      }
    });
  }
});

function signup_user(email, parent) {
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn': 'sign_up_user',
      'email': email
    }
  }).done(function (response) {
    $('.signup_container', parent).removeClass('loading');
    response = $.parseJSON(response);
    $(parent).html('<div class="response-text">' + response.response + '</div>');
    //$('.newsletter_email', parent).val('');
  });
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function setCookie(key, value) {
  var expires = new Date();
  expires.setTime(expires.getTime() + 1268000000); //1 month
  document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
  var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
  return keyValue ? keyValue[2] : null;
}

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map