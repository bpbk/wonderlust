<?php get_header(); ?>
<section id="content" role="main">
<?php
	$hp_args = array('post_type' => 'homepage-section', 'posts_per_page' => -1);
	$hp_query = new WP_Query( $hp_args );
	if ( $hp_query->have_posts() ) :
		while ( $hp_query->have_posts() ) : $hp_query->the_post();
			show_homepage_sections(get_the_id());
		endwhile;
		wp_reset_postdata();
	endif;


	if( have_rows('homepage_layout', get_option('page_on_front')) ): ?>
		<section class="homepage_layout">
		    <?php while ( have_rows('homepage_layout', get_option('page_on_front')) ) : the_row();
		    	switch (get_row_layout()):
			    	case '3_square_images_with_hed':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts): ?>
				    		<div class="inner_content">
				    			<section class="home_layout_section three_squares_with_hed">
					    			<?php foreach($layoutPosts as $layoutPost):
					    			?>
						    			<div id="post_<?php echo $layoutPost; ?>" class="square_post">
								    		<div class="square_post_image">
									    		<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    		<div class="image_sizer">
										    		</div>
										    		<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost),'small-medium' ); ?>
										    		<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">

										    		</div>
								    			</a>
							    			</div>
							    			<div class="square_post_hed post_thumb_hed">
								    			<?php
													post_block_label($layoutPost); ?>
								    			<h2><a href="<?php echo get_the_permalink($layoutPost); ?>"><?php echo wl_title($layoutPost); ?></a></h2>
													<?php
													if(has_excerpt($layoutPost) && !get_field('hide_dek', $layoutPost)) { ?>
														<div class="content_dek<?php echo get_field('all_caps_dek', $layoutPost) ? ' all-caps-dek' : ''; ?>">
															<a href="<?php echo get_the_permalink($layoutPost); ?>">
																<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
															</a>
														</div>
													<?php
													} ?>
							    			</div>
						    			</div>
									<?php endforeach; ?>
				    			</section>
				    		</div>
						<?php endif;
			    	break;
			    	case '3_posts_with_color_hed':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts): ?>
			    			<div class="inner_content">
				    			<section class="home_layout_section three_posts_with_color">
					    			<section class="three_posts_with_color_content">
						    			<?php foreach($layoutPosts as $layoutPost): ?>
							    			<div id="post_<?php echo $layoutPost; ?>" class="color_post">
								    			<div class="square_post_hed post_thumb_hed">
									    			<?php
														post_block_label($layoutPost); ?>
									    			<h2>
										    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
											    			<?php echo wl_title($layoutPost); ?>
										    			</a>
										    		</h2>
									    			<?php if(has_excerpt( $layoutPost ) !== null){ ?>
									    				<div class="content_dek<?php echo get_field('all_caps_dek', $layoutPost) ? ' all-caps-dek' : ''; ?>">
										    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
										    				</a>
									    				</div>
									    			<?php } ?>
								    			</div>
								    			<div class="square_post_image">
									    			<div class="image_sizer">
											    	</div>
											    	<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'small-medium' ); ?>
											    	<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
											    	<a href="<?php echo get_the_permalink($layoutPost); ?>">


											    	</a>
											    	</div>
								    			</div>
							    			</div>
										<?php endforeach; ?>
					    			</section>
					    			<?php if(!my_wp_is_mobile()){ ?>
						    			<section class="placeholder_300">
							    			<?php  echo adrotate_ad(3); ?>
							    		</section>
									<?php } ?>
				    			</section>
			    			</div>
			    			<?php if(my_wp_is_mobile()){ ?>
			    				<section class="placeholder_300">
										<?php  echo adrotate_ad(3); ?>
			    				</section>
			    			<?php } ?>
						<?php endif;
			    	break;
			    	case '4_posts_layout':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts): ?>
			    			<div class="inner_content">
			    				<section class="home_layout_section four_posts_layout">
					    			<?php foreach($layoutPosts as $layoutPost):
							    			$pType = get_post_type( $layoutPost ); ?>
							    			<div id="post_<?php echo $layoutPost; ?>" class="quarter_width_post pType_<?php echo $pType; ?>">
								    			<div class="post_thumb_hed">
									    			<div class="post_hed_sizer">
										    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    			</a>
									    			</div>
									    			<div class="post_thumb_hed_content">
										    			<?php
															post_block_label($layoutPost); ?>
										    			<h2>
											    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
												    			<?php echo wl_title($layoutPost); ?>
											    			</a>
											    		</h2>
										    			<?php if(has_excerpt( $layoutPost ) && $pType == 'page'){ ?>
										    				<div class="content_dek<?php echo get_field('all_caps_dek', $layoutPost) ? ' all-caps-dek' : ''; ?>">
											    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
											    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
											    				</a>
										    				</div>
										    			<?php } ?>
									    			</div>
								    			</div>
								    			<?php if(get_post_type( $layoutPost ) != 'page'){ ?>
									    			<div class="post_image_wrapper">
										    			<div class="image_sizer">
												    	</div>
												    	<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'small-medium' ); ?>
												    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
															<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
															</div>
												    	</a>
									    			</div>
									    			<?php if(has_excerpt( $layoutPost )){ ?>
									    				<div class="content_and_hed">
										    				<?php
																post_block_label($layoutPost); ?>
										    				<h2 class="tablet_size">
												    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
													    			<?php echo wl_title($layoutPost); ?>
												    			</a>
												    		</h2>
										    				<div class="content_dek<?php echo get_field('all_caps_dek', $layoutPost) ? ' all-caps-dek' : ''; ?>">
											    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
											    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
											    				</a>
										    				</div>
									    				</div>
									    			<?php } ?>
								    			<?php } ?>
							    			</div>
										<?php endforeach; ?>
			    				</section>
			    			</div>
						<?php endif;
			    	break;
			    	case 'ad_block':
			    		$adblock = get_sub_field('banner_ad');
			    		if($adblock): ?>
			    			<div class="ad_block">
									<div id='DSK_Leaderboard' style="text-align:center;">
					    			<?php if(!my_wp_is_mobile()){ ?>
										<?php  echo adrotate_ad(4); ?>
					    			<?php }else{ ?>
					    				<!-- /21631492478/hotel_fauchere_300x50 -->
										<?php  echo adrotate_ad(11); ?>
					    			<?php } ?>
									</div>
			    			</div>
			    		<?php
			    		endif;
			    	break;
			    	case 'city_layout':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts):
			    			foreach($layoutPosts as $layoutPost): ?>
				    			<section class="content_block_section city_layout">
								    <?php $cityID = $layoutPost; ?>
								    <?php $sections = get_field('post_sections', $cityID); ?>
								    <div class="inner_content">
										<div class="top_row">
											<div class="top_row_left">
												<div class="top_row_left_content">
													<div class="city_title">
<!--
														<?php if(get_field('associated_city', $cityID)){ ?>
															<h1><a href="<?php echo get_the_permalink($cityID); ?>"><?php echo get_field('associated_city', $cityID); ?></a></h1>
														<?php } ?>
-->
														<h2><a href="<?php echo get_the_permalink($cityID); ?>"><?php echo wl_title($cityID); ?></a></h2>
														<?php
														if(has_excerpt( $cityID )){ ?>
															<div class="city_excerpt<?php echo get_field('all_caps_dek', $cityID) ? ' all-caps-dek' : ''; ?>">
																<a href="<?php echo get_the_permalink($cityID); ?>">
																	<?php echo get_the_excerpt( $cityID ); ?>
																</a>
															</div>
														<?php
														} ?>
													</div>
													<div class="city_image_1">
														<?php $image1 = $sections[0]['post_section_image']['sizes']['small-medium']; ?>
														<a href="<?php echo get_the_permalink($cityID); ?>">
															<img src="<?php echo $image1; ?>"/>
														</a>
													</div>
												</div>
											</div>
											<div class="top_row_right">
												<a href="<?php echo get_the_permalink($cityID); ?>">
													<?php echo get_the_post_thumbnail( $cityID, 'small-medium' ); ?>
												</a>
											</div>
										</div>
										<div class="bottom_row">
											<div class="bottom_row_content">
												<?php
												if(isset($sections[1]['post_section_image'])){ ?>
													<div class="city_image_2">
														<?php
														$image2 = $sections[1]['post_section_image']['sizes']['small-medium']; ?>
														<a href="<?php echo get_the_permalink($cityID); ?>">
															<img src="<?php echo $image2; ?>"/>
														</a>
													</div>
												<?php
												$args = array(
													'post_parent' => $cityID,
													'post_type'   => 'post',
													'numberposts' => -1,
												);
												$children = get_children( $args );
												if(count($children) > 0){ ?>
													<div class="extra_content">
														<strong>PLUS</strong>
														<?php
														foreach($children as $child){ ?>
															<a class="extra_content_link" href="<?php echo get_permalink($child->ID); ?>">
																<?php
																if(get_field('shortened_dek', $child->ID)){
																	echo get_field('shortened_dek', $child->ID);
																}else{
																	echo $child->post_excerpt;
																} ?>
															</a>
														<?php
														} ?>
													</div>
												<?php }
												}
												?>
<!--
												<?php if(get_field('extra_content', $cityID)){ ?>

												<?php } ?>
-->
											</div>
										</div>
								    </div>
								</section>
								<?php
							endforeach;
						endif;
			    	break;
			    	case 'categories_layout':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts): ?>
			    			<div class="inner_content">
				    			<section class="home_layout_section categories_layout">
					    			<?php foreach($layoutPosts as $layoutPost):
						    			$cats = get_the_category( $layoutPost );
					    			?>
						    			<div id="post_<?php echo $layoutPost; ?>" class="category_post">
							    			<h3 class="cat_header_label"><?php echo $cats[0]->name; ?></h3>
							    			<div class="category_post_image">
								    			<div class="image_sizer">
										    	</div>
										    	<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'medium' ); ?>
										    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    		<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
										    		</div>
										    	</a>
							    			</div>
							    			<div class="category_post_hed post_thumb_hed">
								    			<?php
													post_block_label($layoutPost); ?>
								    			<h2>
									    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    			<?php echo wl_title($layoutPost); ?>
									    			</a>
									    		</h2>
								    			<?php if(has_excerpt( $layoutPost ) !== null){ ?>
								    				<div class="content_dek<?php echo get_field('all_caps_dek', $layoutPost) ? ' all-caps-dek' : ''; ?>">
									    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
									    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
									    				</a>
								    				</div>
								    			<?php } ?>
							    			</div>
						    			</div>
									<?php endforeach; ?>
				    			</section>
			    			</div>
						<?php endif;
			    	break;
			    	case 'content_block': ?>
			    		<div class="home_content_block">
				    		<div class="inner_content">
					    		<?php
					    		$headerLabel = get_sub_field('block_label');
					    		if(isset($headerLabel)){ ?>
					    			<div class="content_block_header">
						    			<div class="block_header_left block_line">
						    			</div>
						    			<div class="block_label">
							    			<?php echo get_sub_field('block_label'); ?>
						    			</div>
						    			<div class="block_right block_line">
						    			</div>
					    			</div>
						    	<?php
					    		}
					    		if(get_sub_field('choose_row_layout')){
						    		while ( have_rows('choose_row_layout') ) : the_row();
						    			switch (get_row_layout()):
							    			case 'single_post_layout':
							    				$layoutPosts = get_sub_field('layout_posts');
									    		if($layoutPosts): ?>
									    			<section class="content_block_section single_post_layout <?php echo get_sub_field('content_background'); ?>">
									    			<?php foreach($layoutPosts as $layoutPost): ?>
										    			<div id="post_<?php echo $layoutPost; ?>" class="<?php echo get_sub_field('post_layout'); ?>_container">
											    			<div class="single_post_layout_image post_image_wrapper">
												    			<div class="image_sizer">
														    	</div>
														    	<div class="post_image_container">
															    	<?php if(get_field('embedded_image', $layoutPost)){ ?>
																    	<?php echo get_field('embedded_image', $layoutPost); ?>
															    	<?php }else{ ?>
																    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
													    					<?php echo get_the_post_thumbnail( $layoutPost, 'large' ); ?>
																    	</a>
															    	<?php } ?>
														    	</div>
											    			</div>
											    			<div class="post_thumb_hed">
												    			<?php
																	post_block_label($layoutPost); ?>
												    			<h2>
													    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
														    			<?php echo wl_title($layoutPost); ?>
													    			</a>
													    		</h2>
												    			<?php if(has_excerpt( $layoutPost )){ ?>
												    				<div class="content_dek<?php echo get_field('all_caps_dek', $layoutPost) ? ' all-caps-dek' : ''; ?>">
													    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
													    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
													    				</a>
												    				</div>
												    			<?php } ?>
												    			<?php if(get_field('hotel_name', $layoutPost)){ ?>
												    				<h6 class="hotel_name"><?php echo get_field('hotel_name', $layoutPost); ?></h6>
												    			<?php } ?>
												    			<?php if(get_field('extra_content', $layoutPost)){ ?>
												    				<div class="extra_content"><?php echo get_field('extra_content', $layoutPost); ?></div>
												    			<?php } ?>
											    			</div>
										    			</div>
													<?php endforeach; ?>
										    		</section>
												<?php endif;
							    			break;
							    			case 'two_posts_layout':
							    				$layoutPosts = get_sub_field('layout_posts');
									    		if($layoutPosts): ?>
									    			<section class="content_block_section two_posts_layout">
										    			<?php foreach($layoutPosts as $key=>$layoutPost): ?>
											    			<div id="post_<?php echo $layoutPost; ?>" class="two_posts_layout_<?php echo get_sub_field('2_post_layout'); ?> layout_<?php echo $key; ?>">
												    			<div class="post_image_wrapper">
													    			<div class="image_sizer">
															    	</div>
															    	<?php
																    if(!get_field('3x2_image', $layoutPost)){
																	    $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'medium' );
																	}else{
																		$postImage = get_field('3x2_image', $layoutPost);
																		$postImage = $postImage['sizes']['medium'];
																	} ?>
																    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
																			<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
																			</div>
																    	</a>
												    			</div>
												    			<div class="post_thumb_hed<?php echo get_sub_field('yellow_block') ? ' yellow_bg' : ''; ?>">
													    			<?php
																		post_block_label($layoutPost); ?>
													    			<h3>
														    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
															    			<?php echo wl_title($layoutPost); ?>

														    			</a>
														    		</h3>
														    		<?php if(has_excerpt($layoutPost) && get_sub_field('include_dek')){ ?>
														    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
																			<div class="content_dek<?php echo get_field('all_caps_dek', $layoutPost) ? ' all-caps-dek' : ''; ?>">
																				<?php echo strip_tags(get_the_excerpt($layoutPost)); ?>
																			</div>
														    			</a>
																	<?php } ?>
																	<?php if(get_field('sponsored', $layoutPost) && get_field('sponsored_text', $layoutPost)){ ?>
																		<div class="sponsored_tag"><?php echo get_field('sponsored_text', $layoutPost); ?></div>
																	<?php } ?>

												    			</div>
											    			</div>
														<?php endforeach; ?>
										    		</section>
												<?php endif;
							    			break;
							    			case 'three_posts_layout':
							    				$layoutPosts = get_sub_field('layout_posts');
									    		if($layoutPosts): ?>
									    			<section class="content_block_section three_varied_posts">
									    			<?php foreach($layoutPosts as $layoutPost): ?>
										    			<div id="post_<?php echo $layoutPost; ?>" class="varied_post">
											    			<div class="varied_post_image post_image_wrapper">
												    			<div class="image_sizer">
														    	</div>
														    	<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'small-medium' ); ?>
														    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
																	<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
																	</div>
														    	</a>

											    			</div>
											    			<div class="post_thumb_hed">
												    			<?php
																	post_block_label($layoutPost); ?>
												    			<h3>
													    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
														    			<?php echo wl_title($layoutPost); ?>
													    			</a>
													    		</h3>
												    			<?php if(has_excerpt( $layoutPost )){ ?>
												    				<div class="content_dek<?php echo get_field('all_caps_dek', $layoutPost) ? ' all-caps-dek' : ''; ?>">
													    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
													    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
													    				</a>
												    				</div>
												    			<?php } ?>
											    			</div>
										    			</div>
													<?php endforeach; ?>
										    		</section>
												<?php endif;
							    			break;
							    			default:
							    			break;
						    			endswitch;
						    		endwhile;
						    	} ?>
				    		</div>
			    		</div>
			    	<?php
			    	break;
		    	endswitch;
		    endwhile; ?>
		</section>
    <?php endif; ?>
</section>

<?php get_footer(); ?>
